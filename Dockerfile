FROM --platform=linux/amd64 python:3.9
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code

# Expose HTTP port
EXPOSE 80

CMD ["python3", "-m" ,"uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
