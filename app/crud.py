from sqlalchemy.orm import Session
import models

def get_user_by_username_and_password(db: Session, username: str, password: str, company: int):
    return db.query(models.User).filter(models.User.username == username).filter(models.User.password == password).filter(models.User.company_id == company).first()

def get_user_by_username(db: Session, username: str, company: int):
    return db.query(models.User).filter(models.User.username == username).filter(models.User.company_id == company).first()


def create_user(db: Session,username: str, password: str, company: int):
    db_user = models.User(username=username, password=password, company_id = company)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user