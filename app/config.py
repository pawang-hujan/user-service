import os

class Config:
    def __init__(self):
        self.db_host = os.getenv('DB_HOST')
        self.db_username = os.getenv('DB_USERNAME')
        self.db_password = os.getenv('DB_PASSWORD')
        self.db_name = os.getenv('DB_NAME')
        self.jwt_secret = os.getenv('JWT_SECRET')
        

def get():
    return Config()