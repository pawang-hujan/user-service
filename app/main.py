from fastapi import FastAPI, Depends, status, Response
from sqlalchemy.orm import Session
from models import Base
from request import UserRequest, AuthenticateRequest
from database import engine, SessionLocal
import crud
import jwt
from datetime import datetime, timedelta
from pytz import timezone
import config

Base.metadata.create_all(bind=engine)

app = FastAPI()

cfg = config.get()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/login")
async def login(request: UserRequest, response: Response, db: Session = Depends(get_db)):
    username = request.username
    password = request.password
    company = request.company
    user = crud.get_user_by_username_and_password(db, username, password, company)
    if (user is None):
        response.status_code = 401
        return {"message": "Username and password not match"}
    encoded_jwt = jwt.encode({"username": user.username, "userId": user.id, "exp": datetime.now(tz=timezone('Asia/Jakarta')) + timedelta(hours=24), "companyId": user.company_id}, cfg.jwt_secret, algorithm="HS256")
    return {"accessToken": encoded_jwt, "username": user.username}

@app.post("/register")
async def register(request: UserRequest, response: Response, db: Session = Depends(get_db)):
    username = request.username
    password = request.password
    company = request.company
    user = crud.get_user_by_username(db, username, company)
    if (user is None):
        user = crud.create_user(db, username, password, company)
        response.status_code = status.HTTP_201_CREATED
        return 
    response.status_code = 500
    return {"message": "Failed to create user"}
    
@app.post("/authenticate")
async def login(request: AuthenticateRequest):
    token = request.token
    decoded = jwt.decode(token, cfg.jwt_secret, algorithms=["HS256"])
    return decoded