from pydantic import BaseModel

class AuthenticateRequest(BaseModel):
    token: str

class UserRequest(BaseModel):
    username: str
    password: str
    company: int