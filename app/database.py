from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import config

cfg = config.get()
SQLALCHEMY_DATABASE_URL = f"postgresql://{cfg.db_username}:{cfg.db_password}@{cfg.db_host}/{cfg.db_name}"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()